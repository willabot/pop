<?php
        /* Template Name: Custom Search */        
        get_header(); ?>             
        <div class="contentarea wrap">
            <div id="content" class="woocommerce columns-3">  
                     <h1 class="text-center">Search Result for : <b><?php echo "$s"; ?></b></h1><br /><br />
                     <ul class="products columns-3">
                     <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>    
                     
                     
	<li class="post-34 product type-product status-publish has-post-thumbnail product_cat-tops instock shipping-taxable purchasable product-type-variable has-default-attributes">
    <a href="<?php the_permalink(); ?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">        
    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' );?>
		<img src="<?php  echo $image[0]; ?>" data-id="<?php echo $loop->post->ID; ?>">    
    	<h2 class="woocommerce-loop-product__title"><?php the_title(); ?></h2>
    	<span class="price"><span class="woocommerce-Price-amount amount">
    	<?php echo $product->get_price_html(); ?></span>    
    </a>
    </li>
    
        <?php endwhile; ?>
        <?php endif; ?>
		</ul>
           </div><!-- content -->    
        </div><!-- contentarea -->   
<?php get_footer(); ?>