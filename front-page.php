<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>


<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <div class="home-splash top">
            <div class="hero left">
                <div class="inside">
                    <a href="<?php echo CFS()->get( 'block1_link' ); ?>">
                    	<img class="img-responsive" src="<?php echo CFS()->get( 'block1_img' ); ?>" alt="featured image" width="100%" />
                    </a>
                </div>
            </div>
            <div class="hero right">
                <div class="inside">
                    <a href="<?php echo CFS()->get( 'block2_link' ); ?>">
                        <span class="home-prod-link visible-xs">
                            <?php echo CFS()->get( 'block2_text' ); ?>
                        </span>
                    	<img class="img-responsive" src="<?php echo CFS()->get( 'block2_img' ); ?>" alt="shop new arrivals" width="100%" />
                    	<span class="home-prod-link hidden-xs">
                    		<?php echo CFS()->get( 'block2_text' ); ?>
                    	</span>
                  	</a>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="container">
            <div class="row index-categories">
                <div class="col-sm-4">
                    <a href="<?php echo CFS()->get( 'category1_link' ); ?>"><img class="img-responsive" src="<?php echo CFS()->get( 'category1_img' ); ?>" alt="" width="100%" /><span class="home-prod-link"><?php echo CFS()->get( 'category1_text' ); ?></span></a>
                </div>
                <div class="col-sm-4">
                    <a href="<?php echo CFS()->get( 'category2_link' ); ?>"><img class="img-responsive" src="<?php echo CFS()->get( 'category2_img' ); ?>" alt="" width="100%" /><span class="home-prod-link"><?php echo CFS()->get( 'category2_text' ); ?></span></a>
                </div>
                <div class="col-sm-4">
                    <a href="<?php echo CFS()->get( 'category3_link' ); ?>"><img class="img-responsive" src="<?php echo CFS()->get( 'category3_img' ); ?>" alt="" width="100%" /><span class="home-prod-link"><?php echo CFS()->get( 'category3_text' ); ?></span></a>
                </div>
            </div>
        </div>
        <div class="home-splash bottom">
            <div class="hero left">
                <div class="inside">
                    <a href="<?php echo CFS()->get( 'bottom_link' ); ?>"><img class="img-responsive" src="<?php echo CFS()->get( 'bottom_img' ); ?>" alt="" width="100%" /></a>
                </div>
            </div>
            <div class="hero right">
                <div class="inside">
                    <h2 class="heading-about"><?php echo CFS()->get( 'bottom_text' ); ?></h2>
                    <a class="home-prod-link" href="<?php echo CFS()->get( 'bottom_link' ); ?>">Learn More</a>
                </div>
            </div>
        </div>
        <div class="clear"></div>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
