<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
?>
<div class="woocommerce-variation-add-to-cart variations_button">
	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

	<?php
	do_action( 'woocommerce_before_add_to_cart_quantity' );

	woocommerce_quantity_input( array(
		'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
		'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
		'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
	) );

	do_action( 'woocommerce_after_add_to_cart_quantity' );
	?>
    
    <div class="qty_box_wrapper">
            		<label class="text-center">QTY: <span>1</span>        
                    <ul class="qty_numbers" style="display:none;">
                        <li><a data-id="1" href="javascript:void(0);">1</a></li>
                        <li><a data-id="2" href="javascript:void(0);">2</a></li>
                        <li><a data-id="3" href="javascript:void(0);">3</a></li>
                        <li><a data-id="4" href="javascript:void(0);">4</a></li>
                        <li><a data-id="5" href="javascript:void(0);">5</a></li>
                        <li><a data-id="6" href="javascript:void(0);">6</a></li>
                        <li><a data-id="7" href="javascript:void(0);">7</a></li>
                        <li><a data-id="8" href="javascript:void(0);">8</a></li>          
                    </ul> 
                    </label> 
                    </div><!--qty_box_wrapper-->

	<button type="submit" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

	<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>
