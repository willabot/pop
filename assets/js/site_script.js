$(function() {
	$(".xoo-qv-button").click(function() {
		setTimeout(function() {
			var PopupHeight = $(".xoo-qv-main").height();
			//alert(PopupHeight);
			$(".xoo-qv-summary").css('height', PopupHeight);
			//alert('1');
			$(".qty_box_wrapper").insertAfter("table.variations");
			
			//alert('1');
			//label on category page - in quick view section
  			$("<div class='variant_label'>Color</div>").insertBefore('ul.variable-items-wrapper[data-attribute_name="attribute_pa_color"]');
			$("<div class='variant_label'>Size</div>").insertBefore('ul.variable-items-wrapper[data-attribute_name="attribute_pa_size"]');
			
		}, 1500);
	});
	
	$("body" ).on('click', '.xoo-qv-cls', function() {
		alert('1');
		$('.xoo-qv-opac').click();
	});
	
	
  $(document).click(function(e){
  	if(!$(e.target).is('.qty_box_wrapper label span')) {
    	$('.qty_numbers').hide();
	  }
  });
  
	$(".SearchIcon").click(function() {
		$(".SearchWrapper").fadeToggle('200');
	});
	
	$(".SearchClose").click(function() {
		$(".SearchWrapper").fadeOut('200');
	});
  
  //$(".qty_box_wrapper label span").click(function() {
  $('body').on('click', '.qty_box_wrapper label span', function(){ 
	$(".qty_numbers").toggle();
  });

  $('body').on('click', '.qty_numbers a', function(){ 
 	//alert('1');	
    var reqid = $(this).attr('data-id');
	//alert(reqid);
	$(".input-text.qty").val(reqid);
    $(".qty_numbers").hide();
    var reqval = $(this).html();
    //alert(reqval);
    $(".qty_box_wrapper label span").html(reqval);
  });
  
  $(".related.products").insertAfter('div.single-product');
  //$(".single-product .entry-title").prependTo('.single_inner_info');
  

  //label on product page
  $("<div class='variant_label'>Color</div>").insertBefore('ul.variable-items-wrapper[data-attribute_name="attribute_pa_color"]');
  $("<div class='variant_label'>Select Size</div>").insertBefore('ul.variable-items-wrapper[data-attribute_name="attribute_pa_size"]');
  
  $(".comment-form-comment").insertAfter('.comment-form-title');
  $(".comment-form-rating").insertAfter('.comment-form-title');
  
  $(".summary .woocommerce-product-rating").insertAfter('.header_info');
  //alert('1');
  $("article.type-product").removeClass('product');
  
});


$(document).ready(function(){
	// works on smartphones only
	if($(window).width() < 768){
		// product page image slider
		$(".product_imgs img").wrap('<div>');
		$(".product_imgs").addClass("owl-carousel");
		$(".product_imgs").owlCarousel({
			items:1,
			loop:true,
			stagePadding: 0,
			margin: 10,
			nav:true,
			navText : ["←","→"]
		});

		// submenu conditions
		$( '.menu-item-has-children > a' ).attr("href", "#");

		$( '.menu-item-has-children > a').click(function() {
		  $(this).parents().find('ul.sub-menu').toggle();
		});
	}

	//change logo on scroll
	$(window).scroll(function() {
		if ($(this).scrollTop()>200 && $(window).width() > 768)
		{
			$('.site-header .logo .desktop-version .static').hide();
			$('.site-header .logo .desktop-version .scroll').show();
		}
		else
		{
			$('.site-header .logo .desktop-version .static').show();
			$('.site-header .logo .desktop-version .scroll').hide();
		}
	});
});

