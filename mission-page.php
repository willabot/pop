<?php
/*
*
Template Name: Mission Page
*/

get_header(); ?>


<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <div class="container mission-page">
            <div class="row">
                <div class="col-sm-6">
                    <img class="img-responsive" src="<?php echo CFS()->get( 'mission-image' ); ?>" alt="featured image" width="100%" />
                </div>
                <div class="col-sm-6 text">
                    <div class="row">
                        <div class="col-sm-8">
                            <h1 class="heading-about">
                                <?php echo CFS()->get( 'mission-title' ); ?>
                            </h1>
                        </div>
                    </div>
                    <p>
                        <?php echo CFS()->get( 'mission-desc' ); ?>
                    </p>
                </div>
            </div>
        </div>

        
        


	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
