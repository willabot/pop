<?php
/*
*
Template Name: Account Page
*/
get_header(); ?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
    <div id="account_page">
        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
 
            // Include the page content template.
			the_content();
            //get_template_part( 'template-parts/content', 'page' );
 
            // End of the loop.
        endwhile;
        ?>
 	</div>
    </main><!-- .site-main -->
</div><!-- .content-area -->
<?php get_footer(); ?>