<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="home-splash bottom adventure visible-xs">
		<div class="hero right">
			<div class="inside">
				<img class="img-responsive" src="<?php echo CFS()->get( 'adventure-featured' ); ?>" alt="featured image" width="100%" />
			</div>
		</div>
		<div class="hero left">
			<div class="inside">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<p class="text-center">
					<?php echo CFS()->get( 'adventure-desc' ); ?>
				</p>
			</div>
		</div>
	</div>
	<div class="home-splash bottom adventure hidden-xs">
		<div class="hero left">
			<div class="inside">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<p class="text-center">
					<?php echo CFS()->get( 'adventure-desc' ); ?>
				</p>
			</div>
		</div>
		<div class="hero right">
			<div class="inside">
				<img class="img-responsive" src="<?php echo CFS()->get( 'adventure-featured' ); ?>" alt="featured image" width="100%" />
			</div>
		</div>
	</div>

	<div class="figures">
		<?php 
			$gallery_images = CFS()->get('gallery_images');
			foreach ($gallery_images as $image) {
				echo '<figure class="wp-caption"><img src="'.$image["single-image"].'"/>';
				echo '<figcaption class="wp-caption-text"> '.$image["image-caption"].'</figcaption></figure>';
				
			}
		?>
	</div>

	<?php
	if ( is_single() ) {
		twentyseventeen_entry_footer();
	}
	?>

	<center>
		<p class="goback"><a href="<?php echo site_url(); ?>/adventures/" class="text-center">←Back to Adventures</a></p>
	</center>

</article><!-- #post-## -->
