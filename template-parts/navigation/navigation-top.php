<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<div class="row">
    <div class="logo col-sm-1 col-md-4">
        <a href="<?php echo site_url(); ?>">
        	<span class="hidden-xs hidden-sm desktop-version">
                <img class="static" src="<?php bloginfo('template_directory'); ?>/assets/images/logo.png" width="280" />
                <img class="scroll" src="<?php bloginfo('template_directory'); ?>/assets/images/logo2.png" width="280" style="display: none" />
            </span>
        	<span class="visible-xs visible-sm"><img src="<?php bloginfo('template_directory'); ?>/assets/images/pop-logo-mobile.png" width="45" /></span>
        </a>

    </div>

    <nav id="site-navigation" class="col-xs-12 col-sm-6 col-md-4 main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'twentyseventeen' ); ?>">

        <button class="menu-toggle" aria-controls="top-menu" aria-expanded="false">
        <?php
    		//echo twentyseventeen_get_svg( array( 'icon' => 'bars' ) );
    		//echo twentyseventeen_get_svg( array( 'icon' => 'close' ) );
    		_e( 'Menu', 'twentyseventeen' );
    		?>
            </button>
            <?php wp_nav_menu( array(
    		'theme_location' => 'top',
    		'menu_id'        => 'top-menu',
    	)); ?>


        <?php if ( ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) ) && has_custom_header() ) : ?>
            <a href="#content" class="menu-scroll-down"><?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ); ?><span class="screen-reader-text"><?php _e( 'Scroll down to content', 'twentyseventeen' ); ?></span></a>
        <?php endif; ?>
    </nav>
    <!-- #site-navigation -->

    <div class="right_side_links col-sm-5 col-md-4">
        <ul>
            <?php if ( is_user_logged_in() ) { ?>
                <li>
                    <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>">
                        <?php _e('My Account','woothemes'); ?>
                    </a>
                </li>
                <?php } 
else { ?>
                <li class="hidden-xs">
                    <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','woothemes'); ?>">
                        <?php _e('My Account','woothemes'); ?>
                    </a>
                </li>
                <?php } ?>

                <li class="hidden-xs"><a href="javascript:void(0);"><img src="<?php bloginfo('template_directory'); ?>/assets/images/search_icon.png" class="SearchIcon" alt="search_icon" width="26" /></a></li>
                <li style="position:relative;"><a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/assets/images/cart_icon.png" alt="cart_icon" width="19" /><span><?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?></span></a></li>

        </ul>
    </div>

    <div class="SearchWrapper" style="display:none">
        <div class="page-width" style="position:relative;">
            <?php /*?>
                <?php echo do_shortcode('[aws_search_form]'); ?>
                    <?php */?>

                <form role="search" action="<?php echo site_url('/'); ?>" method="get" class="search-bar__form search search-bar__form woocommerce-product-search" id="searchform">
                    <input type="text" name="s" class="search-field" placeholder="Search Products" />
                    <input type="hidden" name="post_type" value="product" />
                    <!-- // hidden 'products' value -->
                    <input type="submit" alt="Search" value="Search" class="btn_submit" />
                </form>
                <div class="SearchClose">&times;</div>
        </div>
    </div>
   </div>