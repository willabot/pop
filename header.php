<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/pop.css" rel="stylesheet" />
<link type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/bootstrap.min.css" rel="stylesheet" />
<link type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/js/owl/dist/assets/owl.carousel.min.css" rel="stylesheet" />
<link type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/js/owl/dist/assets/owl.theme.default.min.css" rel="stylesheet" />
<link type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/smk-accordion.css" rel="stylesheet" />

<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/js/smk-accordion.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/js/jquery.sticky-kit.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/js/owl/dist/owl.carousel.js"></script>

<script>
$(function() {  
  	$(".accordion-demo").smk_Accordion({
      	showIcon: true, //boolean
  		// The section open on first init.
  		activeIndex : 1,     
  		// Closeable section.
  		closeAble: true, 
  		// Close other sections.
  		closeOther  : true,  
  		// the speed of slide animation.
  		slideSpeed: 200 
	});
	
//$(".summary.entry-summary").stick_in_parent();	

function attach() {
      $(".product .item").stick_in_parent({
      	offset_top: 84 
      });

    }

    attach();

});
</script> 


<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/js/site_script.js"></script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyseventeen' ); ?></a>

	<header id="masthead" class="site-header" role="banner">

		<?php get_template_part( 'template-parts/header/header', 'image' ); ?>

		<?php if ( has_nav_menu( 'top' ) ) : ?>
			<div class="col-md-12">
				<div class="site_header_wrapper">
					<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
				</div><!-- .wrap -->
			</div><!-- .navigation-top -->
		<?php endif; ?>

	</header><!-- #masthead -->

	<?php

	/*
	 * If a regular post or page, and not the front page, show the featured image.
	 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
	 */
	if ( ( is_single() && !in_category('adventures') || ( is_page() && ! twentyseventeen_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() ) ) :
		echo '<div class="single-featured-image-header">';
		echo get_the_post_thumbnail( get_queried_object_id(), 'twentyseventeen-featured-image' );
		echo '</div><!-- .single-featured-image-header -->';
	endif;
	?>

	<div class="site-content-contain">
		<div id="content" class="site-content">
